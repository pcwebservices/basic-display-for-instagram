import os
import redis

from instagram_basic_display.InstagramBasicDisplay import InstagramBasicDisplay # documentation for package - https://pypi.org/project/instagram-basic-display/
from flask import Flask, redirect, request, jsonify

instagram_app_id = os.environ['INSTAGRAM_APP_ID']
instagram_app_secret = os.environ['INSTAGRAM_APP_SECRET']
instagram_redirect_url = 'https://pc-basic-display.azurewebsites.net/callback'
redis_host=os.environ['REDIS_HOST']
redis_port=os.environ['REDIS_PORT']
redis_password=os.environ['REDIS_PASSWORD']

# dev
# from dotenv import load_dotenv
# APP_ROOT = os.path.join(os.path.dirname(__file__), '..')   # refers to application_top
# dotenv_path = os.path.join(APP_ROOT, '.env')
# load_dotenv(dotenv_path)
# instagram_app_id = os.getenv('INSTAGRAM_APP_ID')
# instagram_app_secret = os.getenv('INSTAGRAM_APP_SECRET')
# instagram_redirect_url = 'http://localhost:5000/callback'
# redis_host=os.getenv('REDIS_HORT')
# redis_post=os.getenv('REDIS_PORT')
# redis_password=os.getenv('REDIS_PASSWORD')

# to activate venv: source .venv/Scripts/activate

app = Flask(__name__)


@app.route("/", methods = ['GET'])
def profile():
    user = request.args.get('user')  

    # if there is no user on the query string
    if user == None:
        return jsonify({"message": "Please request a user's profile"})

    # try:
    redirect_url = instagram_redirect_url 
    redirect_url = redirect_url + '/' + user
    instagram_basic_display = InstagramBasicDisplay(app_id=instagram_app_id, app_secret=instagram_app_secret, redirect_url=redirect_url)

    # connect to redis cache
    r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, ssl=True)
    # get token from cache
    user_id = r.get(user + '_id')
    token = r.get(user + '_token')

    if user_id != None:
        #  Set user access token
        instagram_basic_display.set_access_token(token)
        # Get the users profile
        profile = instagram_basic_display.get_user_media(user_id=user_id.decode('utf-8'), limit=3)
        return jsonify(profile)
    else:
        return auth()          



@app.route("/auth",  methods = ['GET'])
def auth():
    user = request.args.get('user')

    redirect_url = instagram_redirect_url 
    
    if( user != None ):
        redirect_url = redirect_url + '/' + user

    instagram_basic_display = InstagramBasicDisplay(app_id=instagram_app_id, app_secret=instagram_app_secret, redirect_url=redirect_url)
    url = instagram_basic_display.get_login_url()   
    return redirect(url)

@app.route("/callback/<user>", methods = ['GET'])
def callback(user):
    
    # Get the OAuth callback code
    code = request.args.get('code')

    if user == None:
        return jsonify({"message": "You need to provide a user"})

    redirect_url = instagram_redirect_url 
    
    if( user != None ):
        redirect_url = redirect_url + '/' + user

    instagram_basic_display = InstagramBasicDisplay(app_id=instagram_app_id, app_secret=instagram_app_secret, redirect_url=redirect_url)

    # Get the short lived access token (valid for 1 hour)
    short_lived_token = instagram_basic_display.get_o_auth_token(code)

    user_id=short_lived_token.get('user_id')

    # Exchange this token for a long lived token (valid for 60 days)
    long_lived_token = instagram_basic_display.get_long_lived_token(short_lived_token.get('access_token'))

    access_token = long_lived_token.get('access_token')

    FIFTY_NINE_DAYS_IN_SECONDS = 59 * 24 * 60 * 60
    #FIFTY_NINE_DAYS_IN_SECONDS = 5 * 60 # set to five minutes for debugging purposes
    
    if(long_lived_token):
        r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password,ssl=True)
        r.set(user + '_id', user_id)
        r.setex(user + '_token', FIFTY_NINE_DAYS_IN_SECONDS, access_token)
        return jsonify({"message": "Your account has been authorized"})
    
    return jsonify({"message": "Sorry, something went wrong"})


@app.route("/deauthorize", methods=['GET'])
def deauthorize():
    return "Hello"

@app.route("/delete", methods=['GET'])
def delete():
    return "Hello"

